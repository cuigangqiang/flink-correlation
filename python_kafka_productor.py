import json
from kafka import KafkaProducer

producer = KafkaProducer(bootstrap_servers='192.168.49.129:9092')
msg_dict = {
    "sleep_time": 10,
    "db_config": {
        "database": "test_1",
        "host": "xxxx",
        "user": "root",
        "password": "root"
    },
    "table": "msg",
    "msg": "nishi🐖猪"
    ,'msg':'好样的'
}
msg = json.dumps(msg_dict)
producer.send('test', msg.encode(), partition=0)
producer.close()
